package si.ijs.e6;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import si.ijs.e6.S2.AbsoluteId;
import si.ijs.e6.S2.DataEntityCache;
import si.ijs.e6.S2.ReadLineCallbackInterface;
import si.ijs.e6.S2.SensorDefinition;
import si.ijs.e6.S2.StructDefinition;
import si.ijs.e6.S2.TimestampDefinition;
import si.ijs.e6.S2.ValueType;

/**
 * 
 * Callback interface implemented to process data for octave
 * 
 */

public class CallbackOctaveOutput implements ReadLineCallbackInterface	//Only data output
{
	private ArrayList<Float[]> outputData;
	private ArrayList<Long> outputNanoseconds;
	private S2 s2;
	
	CallbackOctaveOutput(S2 file){
		outputData = new ArrayList<>();
		outputNanoseconds = new ArrayList<>();
		s2 = file;
	}
	
	public ArrayList<Float[]> getOutputData()
	{
		return outputData;
	}
	
	public ArrayList<Long> getOutputNanoseconds()
	{
		return outputNanoseconds;
	}
	
	public boolean onComment(String comment)
	{
		return true;
	}
    
	public boolean onVersion(int versionInt, String version)
    {
    	return true;
    }
	
	public boolean onSpecialMessage(char who, char what, String message)
	{
		long maxTimestamp = 0;
		for(DataEntityCache c : s2.getCachedHandles())
		{
			if(c.lastAbsTimestamp.getValue() > maxTimestamp)
				maxTimestamp = c.lastAbsTimestamp.getValue();
		}
		
		double toSeconds = maxTimestamp / 1000000000.0;
    	//double toSeconds = s2.getCachedHandles()/1000000000.0;	// Converts nanoseconds into seconds
    	String seconds = String.valueOf(Math.round(toSeconds * 100.0) / 100.0);	// Rounds to two decimal places
    	
    	String device = "";
    	String messageType = "";
    	switch(who)		// Converts byte into human readable string 
    	{
    	case 1:
    		device = "The sensor device";
    		break;
    	case 2:
    		device = "The recording device";
    		break;
    	case 3:
    		device = "The editing application";
    		break;
    	default:
    		device = "note from sensing device";
    	}
    	
    	switch(what)	// Converts byte into human readable string 
    	{
    	case 'w':
    		messageType = "Warning";
    		break;
    	case 'e':
    		messageType = "Error";
    		break;
    	case 'x':
    		messageType = "Excpetion";
    		break;
    	case 'd':
    		messageType = "Debug message";
    		break;
    	case 'a':
    		messageType = "Annotation";
    		break;
    	case 'n':
    		messageType = "User-defined note";
    		break;
    	}
    	
		//System.out.println(String.format("%12s %15s : %s: %s" , seconds + "s", "special message", device, message ));
		return true;
	}
	
	String absDate = "";
	String absTime = "";
	String absTimezone = "";
	long nanoseconds = 0;
	public boolean onMetadata( String key,  String value)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		
		switch (key)
		{
		case "date":
			absDate = value;
			break;
		case "time":
			absTime = value;
			break;
		case "timezone":
			absTimezone = value;
			break;
		}
		 
		Date date = null;
		if (!absDate.equals("") && !absTime.equals("") && !absTimezone.equals(""))
		{
			//sdf.setTimeZone(TimeZone.getTimeZone(absTimezone));
			try {
				date = sdf.parse(absDate + " " + absTime);
				nanoseconds = date.getTime(); 	//from milliseconds to nanoseconds
				long l = date.getTime();
				//Date dt = new Date(date.getTime());	Test
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		return true;
	}
	
	public boolean onEndOfFile()
	{
		return true;
	}

	@Override
	public boolean onUnmarkedEndOfFile() {
		return true;
	}
	
	public boolean onDefinition(byte handle, SensorDefinition definition)
	{  	
    	ValueType vt;
    	switch(definition.valueType)
    	{
    	case 99:	// c is present as 99 in ascii table
    		vt = ValueType.vt_char;
    		break;
    	case 102:	// f  is present as 102 in ascii table
    		vt = ValueType.vt_float;
    		break;
    	case 105:	// i is present as 105 in ascii table
    		vt = ValueType.vt_integer;
    		break;
    	default:
    		vt = ValueType.vt_invalid;
    		break;
    	}
    	
    	AbsoluteId ai;
    	switch(definition.absoluteId)
    	{
    	case 97:	// a  is present as 97 in ascii table
    		ai = AbsoluteId.abs_absolute;
    		break;
    	case 114:	// r is present as 114 in ascii table
    		ai = AbsoluteId.abs_relative;
    		break;
    	default:
    		ai = AbsoluteId.abs_invalid;
    		break;
    	}
    	
    	String vectorSize = "";
    	if (definition.vectorSize == 1)
    		vectorSize = "scalar";
    	else
    		vectorSize = String.valueOf(definition.vectorSize);
    	
		//System.out.println(String.format("%28s : %3s [%s] %s; name=%s, unit=%s, %s bits/sample + %s bits of padding, type=%s %s, %s, with additional %s bits of padding, f_s=%s, y=%s*x-%s", "definition", handle, (char)handle, "sensor", definition.name, definition.unit, definition.resolution, definition.scalarBitPadding, vt, ai, vectorSize, definition.vectorBitPadding, definition.samplingFrequency, definition.k, definition.n ));
		return true;
	}
	
	public boolean onDefinition(byte handle, StructDefinition definition)
	{
		//System.out.println(String.format("%28s : %3s [%s] %s; name=%s, elements=[%s]" , "definition", handle, (char)handle, "struct", definition.name, definition.elementsInOrder ));
		return true;
	}
	
	public boolean onDefinition(byte handle, TimestampDefinition definition)
	{
		AbsoluteId ai;
    	switch(definition.absoluteId)
    	{
    	case 97:	// a  is present as 97 in ascii table
    		ai = AbsoluteId.abs_absolute;
    		break;
    	case 114:	// r is present as 114 in ascii table
    		ai = AbsoluteId.abs_relative;
    		break;
    	default:
    		ai = AbsoluteId.abs_invalid;
    		break;
    	}
    	
		//System.out.println(String.format("%28s : %3s [%s] %s; %s, size=%s B, resolution=%s s", "definition", handle, (char)handle, "timestamp", ai, definition.byteSize, definition.multiplier));
		return true;
	}
	
	public boolean onTimestamp(long nanoSecondTimestamp)
	{
		long maxTimestamp = 0;
		for(DataEntityCache c : s2.getCachedHandles())
		{
			if(c.lastAbsTimestamp.getValue() > maxTimestamp)
				maxTimestamp = c.lastAbsTimestamp.getValue();
		}
		
		double toSeconds = maxTimestamp / 1000000000.0;
    	//double toSeconds = s2.getCachedHandles()/1000000000.0;	// Converts nanoseconds into seconds
    	String seconds = String.valueOf(Math.round(toSeconds * 100.0) / 100.0);	 // Rounds to two decimal places
		//System.out.println(String.format("%12s %15s : %s ns" , seconds + "s", "timestamp", nanoSecondTimestamp ));
		return true;
	}
	
	public boolean onStreamPacket(byte handle, long timestamp, int len, byte data[])
	{
    	double toSeconds = timestamp/1000000000.0;	// Converts nanoseconds into seconds
    	String seconds = String.valueOf(Math.round(toSeconds * 100.0) / 100.0);	 // Rounds to two decimal places
    	
    	ArrayList<Float> sensorData = new ArrayList<>();
    	MultiBitBuffer mbb = new MultiBitBuffer(data);	// Getting 10-bit sample from 19-Bytes
        for (int i = 0; i < 14; ++i) {
            int temp = mbb.getInt(i*10, 10);
            sensorData.add(temp*1.0f);
        }
        int tenBitTime = mbb.getInt(14*10, 10); // 10-bit sample counter
    	
        //GetData************************
        Object[] objData = sensorData.toArray();
        Float[] dataArray = Arrays.copyOf(objData, objData.length, Float[].class);
        Float[] packetData = {(float)tenBitTime};
        
        Float[] measuredData = new Float[packetData.length + dataArray.length];
        
        System.arraycopy(packetData, 0, measuredData, 0, packetData.length);
        System.arraycopy(dataArray, 0, measuredData, packetData.length, dataArray.length);
        
        outputData.add(measuredData);
        //*******************************
                    
        
        //GetNanoseconds*****************
        double freqNanoseconds = 1000000000 / 125.0687; //Nanoseconds
        //double tempNanoseconds = nanoseconds + timestamp;
        nanoseconds = timestamp;
        
        long milliseconds = (long)nanoseconds/1000000;
        Date dt = new Date(milliseconds);
        //System.out.println("Before***** " + dt + " - Milliseconds: " + milliseconds);
        for(int i = 0; i < 14; ++i)
        {
        	long n = nanoseconds+(long)(i*freqNanoseconds);
        	Date ddt = new Date((long)(n)/1000000);
        	//System.out.println("\t new time = " + ddt);
        	outputNanoseconds.add(n);
        }
		return true;
	}
	
//	public boolean onStreamPacket(byte handle, long timestamp, byte len, byte data[])
//	{
//    	double toSeconds = timestamp/1000000000.0;	// Converts nanoseconds into seconds
//    	String seconds = String.valueOf(Math.round(toSeconds * 100.0) / 100.0);	 // Rounds to two decimal places
//    	
//    	ArrayList<Float> sensorData = new ArrayList<>();
//    	MultiBitBuffer mbb = new MultiBitBuffer(data);	// Getting 10-bit sample from 19-Bytes
//        for (int i = 0; i < 14; ++i) {
//            int temp = mbb.getInt(i*10, 10);
//            sensorData.add(temp*1.0f);
//        }
//        int tenBitTime = mbb.getInt(14*10, 10); // 10-bit sample counter
//    	;
//		//System.out.println(String.format("%12s %15s : handle=%s, raw timestamp=%s, sample counter=%s, samples=%s" , seconds + "s", "stream packet", handle, timestamp, tenBitTime, sensorData));
//        Object[] objData = sensorData.toArray();
//        Float[] dataArray = Arrays.copyOf(objData, objData.length, Float[].class);
//        Float[] packetData = {(float)tenBitTime};
//        
//        Float[] measuredData = new Float[packetData.length + dataArray.length];
//        
//        System.arraycopy(packetData, 0, measuredData, 0, packetData.length);
//        System.arraycopy(dataArray, 0, measuredData, packetData.length, dataArray.length);
//        
//        
//        //Ideja za timestamp je, da tu dobim timestamp in 14 meritev. Dodam prvi timestamp k prvi meritvi, ostale potem naračunam po formuli.
//        //TenBitTime torej ne rabim na začetku tabele.
//        
//        //outputSeconds.add(seconds);
//        outputData.add(measuredData);
//		return true;
//	}
	
	public boolean onUnknownLineType(byte type, int len, byte data[])
	{
		long maxTimestamp = 0;
		for(DataEntityCache c : s2.getCachedHandles())
		{
			if(c.lastAbsTimestamp.getValue() > maxTimestamp)
				maxTimestamp = c.lastAbsTimestamp.getValue();
		}
		
		double toSeconds = maxTimestamp / 1000000000.0;
    	//double toSeconds = s2.getCachedHandles()/1000000000.0;	// Converts nanoseconds into seconds
    	String seconds = String.valueOf(Math.round(toSeconds * 100.0) / 100.0);	 // Rounds to two decimal places
    	
    	ArrayList<Float> sensorData = new ArrayList<>();
    	MultiBitBuffer mbb = new MultiBitBuffer(data);	// Getting 10-bit sample from 19-Bytes
        for (int i = 0; i < 14; ++i) {
            int temp = mbb.getInt(i*10, 10);
            sensorData.add(temp*1.0f);
        }
        int tenBitTime = mbb.getInt(14*10, 10); // 10-bit sample counter
    	
		System.out.println(String.format("%12s %15s : type=%s [%s], sample counter=%s, samples=%s" , seconds + "s", "unknown", type, (char)type, tenBitTime, sensorData ));
		return true;
	}
	
	public boolean onError(int lineNum,  String error)
	{
		return true;
	}
}
